# Renovate Config

Our renovate config presets

## Getting started

`renovate.json5`
```jsonc
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>txlab/ops/renovate-config" // https://gitlab.com/txlab/ops/renovate-config
  ]
}
```

## Other presets
- Javascript: [`gitlab>txlab/ops/renovate-config:js`](./js.json)


## Extras
### Dockerfile custom arg resolver
In `Dockerfile` or `*.dockerfile`, add a comment above an `ARG` or `ENV` with [`datasource`](https://docs.renovatebot.com/modules/manager/regex/) and `depName`, optionally [`versioning`](https://docs.renovatebot.com/modules/versioning/).
```Dockerfile
 # renovate: datasource=github-releases depName=composer/composer
ENV COMPOSER_VERSION=1.9.3
 # renovate: datasource=github-tags depName=nodejs/node versioning=node
ARG NODE_VERSION=10.19.0
```
inspired by: [renovate docs](https://docs.renovatebot.com/modules/manager/regex/#advanced-capture)
